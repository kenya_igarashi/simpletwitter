package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {

	private int messageId;
	private String account;
	private String name;
	private String commentText;
	private Date commentCreatedDate;

	public int getMessageId() {
		return messageId;
	}

	public String getAccount() {
		return account;
	}

	public String getName() {
		return name;
	}

	public String getCommentText() {
		return commentText;
	}

	public Date getCommentCreatedDate() {
		return commentCreatedDate;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public void setCommentCreatedDate(Date commentCreatedDate) {
		this.commentCreatedDate = commentCreatedDate;
	}
}