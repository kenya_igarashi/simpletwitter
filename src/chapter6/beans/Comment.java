package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {

	private int messageId;
	private int userId;
	private String commentText;
	private Date commentCreatedDate;
	private Date commentUpdatedDate;

	public int getMessageId() {
		return messageId;
	}

	public int getUserId() {
		return userId;
	}

	public String getCommentText() {
		return commentText;
	}

	public Date getCreatedDate() {
		return commentCreatedDate;
	}

	public Date updatedDate() {
		return commentUpdatedDate;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public void setCreatedDate(Date commentCreatedDate) {
		this.commentCreatedDate = commentCreatedDate;
	}

	public void setUpdatedDate(Date commentUpdatedDate) {
		this.commentUpdatedDate = commentUpdatedDate;
	}
}